'use strict'

const Schema = use('Schema')

class LoadTypesSchema extends Schema {
  up () {
    this.create('load_types', (table) => {
      table.increments()
      table.string('description', 80).notNullable().unique()
    })
  }

  down () {
    this.drop('load_types')
  }
}

module.exports = LoadTypesSchema
