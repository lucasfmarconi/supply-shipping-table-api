'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.group(() => {
  Route.post('shipping', 'ShippingController.create')
  Route.post('shipping/calculate', 'ShippingController.calculate')
  Route.put('shipping/:id', 'ShippingController.update')
  Route.get('shipping', 'ShippingController.index')
  // Route.get('shipment/:id', 'ShippingController.show')
}).prefix('api/')

Route.group(() => {
  Route.get('loadtype', 'LoadTypeController.index')
}).prefix('api/')
