'use strict'

const Model = use('Model')

class LoadType extends Model {
  shippings() {
    return this.belongsToMany('App/Models/Shipping')
  }
}

module.exports = LoadType
