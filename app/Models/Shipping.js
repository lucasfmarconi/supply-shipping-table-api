'use strict'

const Model = use('Model')

class Shipping extends Model {
  static boot() {
    super.boot()
  }

  loadType() {
    return this.hasOne('App/Models/LoadType')
  }
}

module.exports = Shipping
