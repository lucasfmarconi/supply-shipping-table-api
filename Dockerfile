FROM mhart/alpine-node:10

WORKDIR /app

COPY package*.json ./

COPY . .

RUN npm install sqlite3 --save
RUN npm install -g @adonisjs/cli

EXPOSE 3333

CMD [ "npm", "start" ]